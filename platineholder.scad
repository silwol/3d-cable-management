foot_length = 15;
foot_thickness = 2;
wall_thickness = 4;
base_distance = 15;
holder_width = 50;

notch_depth = 2;
notch_width_inner = 2;
notch_width_extra = 0.5;

holder_height = 1;

borehole_diameter = 4;
borehole_inset = 6;

alittle = 0.001;

module body() {
  linear_extrude(height = holder_width) {
    polygon([
      [0,0],
      [foot_length, 0],
      [foot_length, foot_thickness],
      [wall_thickness, foot_thickness],
      [wall_thickness, base_distance - notch_width_extra],
      [wall_thickness - notch_depth, base_distance],
      [wall_thickness - notch_depth, base_distance + notch_width_inner],
      [wall_thickness, base_distance + notch_width_inner + notch_width_extra],
      [wall_thickness, base_distance + notch_width_inner + notch_width_extra + holder_height],
      [0, base_distance + notch_width_inner + notch_width_extra + holder_height],
    ]);
  }
}

module borehole() {
  screw_recess_depth = foot_thickness / 3 * 2;
  cylinder(d = borehole_diameter, h = foot_thickness + 2 * alittle, $fn = 100);
  translate([0, 0, foot_thickness - screw_recess_depth]) {
    cylinder(
      d1 = borehole_diameter,
      d2 = borehole_diameter + screw_recess_depth * 2,
      h = screw_recess_depth + 2 * alittle,
      $fn = 100);
  }
}

difference() {
  body();
  translate([(foot_length + wall_thickness)/2, -alittle, borehole_inset]) {
    rotate([-90, 0, 0]) {
      borehole();
    }
  }
  translate([(foot_length + wall_thickness)/2, -alittle, holder_width - borehole_inset]) {
    rotate([-90, 0, 0]) {
      borehole();
    }
  }
}
