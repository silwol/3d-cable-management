alittle = 0.001;

cover_diameter = 70;
cover_thickness = 5;

roll_diameter = 30;
roll_width = 25;
roll_thickness = 10;

hole_diameter = 4;
nut_diameter = 8.2;
nut_hole_depth = cover_thickness - 1;

base_thickness = 2;
base_hole_diameter = 4;

num_holes = 12;

$fn = 600;

module screwhole() {
    cylinder(d=hole_diameter, h=cover_thickness + 2 * alittle);
    translate([0, 0, cover_thickness - nut_hole_depth]) {
        cylinder(d = nut_diameter, h = nut_hole_depth + 2 * alittle, $fn = 6);
    }
}

difference() {
    cylinder(d = cover_diameter, h = cover_thickness);
    translate([0, 0, -alittle]) {
        cylinder(d = roll_diameter, h = cover_thickness + 2 * alittle);
    }
    for (i = [1 : num_holes]) {
        rotate([0, 0, i * 360 / num_holes]) {
            translate([0, (roll_diameter + (cover_diameter - roll_diameter) / 2) / 2, -alittle]) {
                screwhole();
            }
        }
    }
}

difference() {
    cylinder(d = roll_diameter, h = roll_width);
    translate([0, 0, -alittle]) {
        cylinder(d=roll_diameter - roll_thickness * 2, h=roll_width + 2 * alittle);
    }
}

translate([0, 0, roll_width - base_thickness]) {
    difference() {
        cylinder(d = roll_diameter, h = base_thickness);
        translate([0, 0, -alittle]) {
            cylinder(d = base_hole_diameter, h = base_thickness + 2 * alittle);
        }
    }
}
