# 3D models for cable management items

## Clip

A clip for clamping cables in between. The clip can be mounted either with
a screw through the hole, or with a two-sided adhesive tape.

![clip render](images/clip-render.png "Clip render")

## Spool

A spool that can be mounted on a surface using a screw. It can be used to
wind cables around it. The clip described above can be mounted on it using
a M4 x 10 screw and a self-locking M4 nut.

![spool render](images/spool-render.png "Spool render")

# License

The source code for the models is licensed under the AGPLv3 or later.
