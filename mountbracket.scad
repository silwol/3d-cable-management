wall_thickness = 1.5;
item_height = 25;
bracket_length = 20;
screwhole_diameter = 3;

alittle = 0.001;

$fn=100;

difference() {
    cube([bracket_length,item_height + wall_thickness * 2,bracket_length]);
    translate([wall_thickness, wall_thickness, wall_thickness]) {
        cube([bracket_length, item_height, bracket_length]);
    }

    translate([
        bracket_length / 2,
        item_height + wall_thickness - alittle,
        bracket_length / 2
    ])
    {
        rotate([-90,0,0]) {
            cylinder(d=screwhole_diameter, h=wall_thickness + 2* alittle);
        }
        rotate([-90,0,0]) {
            d2=screwhole_diameter;
            d1=wall_thickness + d2;
            cylinder(d1=d1, d2=d2, h=wall_thickness / 2);
        }
    }

    translate([bracket_length/2, -alittle, bracket_length/2]) {
        rotate([-90,0,0]) {
            cylinder(d=bracket_length-7, h=wall_thickness + 2*alittle);
        }
    }

    translate([alittle, -alittle, alittle]) {
        difference() {
            cube([
                bracket_length,
                item_height + wall_thickness * 2 +  2 * alittle,
                bracket_length
            ]);
            translate([
                bracket_length / 2,
                -alittle,
                bracket_length / 2
            ])
            {
                rotate([-90,0,0]) {
                    cylinder(
                        d = bracket_length,
                        h=item_height + 2 * wall_thickness + 2 * alittle
                    );
                }
            }
            translate([-alittle, -alittle, -alittle]) {
                cube([
                    bracket_length / 2 + alittle,
                    item_height + 2 * wall_thickness + 2 * alittle,
                    bracket_length + 2 * alittle
                ]);
                cube([
                    bracket_length + 2 * alittle,
                    item_height + 2 * wall_thickness + 2 * alittle,
                    bracket_length / 2 + 2 * alittle
                    ]);
            }
        }
    }
}
