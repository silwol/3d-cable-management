alittle = 0.001;

outer_width = 82;
outer_height = 90;
outer_depth = 22;

inner_width = 75;
inner_depth = 15;
inner_height = 85;

holder_width = 70;

cable_hole_width = 20;

borehole_diameter = 4;
borehole_inset = 15;

backside_thickness = (outer_depth - inner_depth) / 2;

module borehole() {
  screw_recess_depth = backside_thickness / 3 * 2;
  cylinder(d = borehole_diameter, h = backside_thickness + 2 * alittle, $fn = 100);
  translate([0, 0, backside_thickness - screw_recess_depth]) {
    cylinder(
      d1 = borehole_diameter,
      d2 = borehole_diameter + screw_recess_depth * 2,
      h = screw_recess_depth + 2 * alittle,
      $fn = 100);
  }
}


difference() {
  // The body
  cube([outer_width, outer_depth, outer_height]);

  // Put a large hole into the body, where the phone will sit
  translate([
    (outer_width - inner_width) / 2,
    (outer_depth - inner_depth) / 2,
    outer_height - inner_height
  ]) {
    cube([inner_width, inner_depth, inner_height + alittle]);
  }

  // Remove the front plate
  translate([
    (outer_width - holder_width) / 2,
    -alittle,
    outer_height - inner_height,
  ]) {
    cube([
      holder_width,
      (outer_depth - inner_depth) / 2 + 2 * alittle,
      inner_height + alittle
    ]);
  }

  // Make a hole for the charging cable
  translate([
    (outer_width - cable_hole_width) / 2,
    -alittle,
    -alittle
  ]) {
    cube([
      cable_hole_width,
      outer_depth - backside_thickness + alittle,
      outer_height - inner_height + 2 * alittle
    ]);
  }
    
  // Bore holes
  {
    translate([borehole_inset, outer_depth + alittle, borehole_inset]) {
      rotate([90, 0, 0]) {
        borehole();
      }
    }
      
    translate([borehole_inset, outer_depth + alittle, outer_height - borehole_inset]) {
      rotate([90, 0, 0]) {
        borehole();
      }
    }
      
    translate([outer_width - borehole_inset, outer_depth + alittle, borehole_inset]) {
      rotate([90, 0, 0]) {
        borehole();
      }
    }
      
    translate([outer_width - borehole_inset, outer_depth + alittle, outer_height - borehole_inset]) {
      rotate([90, 0, 0]) {
        borehole();
      }
    }
  }
}
