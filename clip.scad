alittle = 0.001;

r = 15;
thickness = 1;
width = 15;
straight_length = 10;

stand_distance = 12;
base_length = stand_distance + 4;
base_height = 1;
stand_angle = 25;
hole_diameter = 4;

module stand_rawposition() {
    translate([-r,straight_length,0]) {
        intersection() {
            translate([r, r, width/2]) {
                rotate([90,0,0]) {
                    cylinder(h=r*2, r=width*0.8);
                }
            }
            intersection() {
                cube([r,r,width]);
                difference() {
                    cylinder(h=width,r=r,$fn=1000);
                    translate([0,0,-alittle]) {
                        cylinder(h=width+2*alittle,r=r-thickness,$fn=1000);
                    }
                }
            }
        }

        translate([r-thickness,-straight_length,0]) {
            cube([thickness,straight_length,width]);
        }
    }
}

module stand() {
    translate([-stand_distance/2, 0, 0]) {
        rotate([0,0,-stand_angle]) {
            stand_rawposition();
        }
    }
}

stand();
translate([0, 0, width]) {
    rotate([0, 180,0]) {
        stand();
    }
}

difference() {
translate([-base_length/2, 0, 0]) {
    cube([base_length, base_height, width]);
}
translate([0, base_height + alittle, width / 2]) {
    rotate([90,0,0]) {
        cylinder(h=base_height + 2*alittle,d=hole_diameter,$fn=1000);
    }
}
}
